var content = [
  { name: "name1", text: "text1" },
  { name: "name2", text: "text2" },
  { name: "name3", text: "text3" },
];

let callBackResult = document.querySelector(".callback-result");
/// callback method ///

//// bu yöntemde callback belli bir yerde çalışan fonksiyonun ya da api call un bitmesini beklemek ve sonra çalışmak için koyulan checkpoint gibi çalışıyor///

function setItem() {
  let element = "";
  setTimeout(() => {
    content.forEach((item) => {
      element += `
              <b>${item.name}</b>
              <p>${item.text}</p>
              `;
    });
    callBackResult.innerHTML = element;
  }, 2000);
}

function pushObj(obj, callback) {
  setTimeout(() => {
    obj.push({
      name: "name4",
      text: "text4",
    });
    callback();
  }, 4000);
}
pushObj(content, setItem);
setItem();

///Promise medhod///
/// promise asenkron olarak işlem tamamlandıktan sonr şuradan devam edebilirsin diyen mekanizma kısaca///

let promiseResult = document.querySelector(".promise-result");

var content_second = [
  { name: "name_second1", text: "text_second1" },
  { name: "name_second2", text: "text_second2" },
  { name: "name_second3", text: "text_second3" },
];

function setPromise() {
  let promiseElement = "";
  setTimeout(() => {
    content_second.forEach((item) => {
      promiseElement += `
                <b>${item.name}</b>
                <p>${item.text}</p>
                `;
    });
    promiseResult.innerHTML = promiseElement;
  }, 2000);
}

function newItems(obj) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      content_second.push(obj);
      const error = false;
      if (!error) {
        resolve(content_second);
      } else {
        reject("Hata var...");
      }
    }, 4000);
  });
}

newItems({
  name: "name_second4",
  text: "text_second4",
}).then((responseData) => {
    console.log("yeni liste",responseData);
  setPromise();
}).catch(error => {
    console.log(error);
})
setPromise();

//PromiseAll//
/// promise all atacağımız listeyi arry olarak atıyor olumlu cevap geldiğnde geri dönüş yapıyor //

const promise1 = Promise.resolve("ı am a first value")
const promise2 = new Promise((resolve,reject) => {
setTimeout(()=> {
    resolve("ı am a second value")
},2000)
})

const promise3 = fetch("https://jsonplaceholder.typicode.com/posts").then(res=>res.json())

Promise.all([promise1,promise2,promise3]).then(responseData => {
    console.log("data",responseData);
})
